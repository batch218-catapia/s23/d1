// console.log("Hello World");

// [Section] Objects
/*
    - An object is a data type that is used to represent real world objects
    - It is a collection of related data and/or functionalities
    - In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
    - Information stored in objects are represented in a "key:value" pair
    - A "key" is also mostly referred to as a "property" of an object
- Different data types may be stored in an object's property creating complex data structures
*/


// Creating objects using object initializers/literal notation

/*
    - This creates/declares an object and also initializes/assigns it's properties upon creation
    - A cellphone is an example of a real world object
    - It has it's own properties such as name, color, weight, unit model and a lot of other things
    - Syntax
        let objectName = {
            keyA: valueA,
            keyB: valueB
        }
*/


let cellphone = {
    name: 'Nokia 3210',
    manufactureDate: 1999
};

console.log(cellphone);
console.log(cellphone.toString());

console.log('Result from creating objects using initializers/literal notation:');
console.log(cellphone);
console.log(typeof cellphone);


// Creating objects using a constructor function
/*
    - Creates a reusable function to create several objects that have the same data structure
    - This is useful for creating multiple instances/copies of an object
    - An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
    - Syntax
        function ObjectName(keyA, keyB) {
            this.keyA = keyA;
            this.keyB = keyB;
        }
*/

// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters
function Laptop(name, manufactureDate){
    this.name = name;
    this.manufactureDate = manufactureDate;
}

let lenovoLaptop = new Laptop("Lenovo", 2001);



console.log(lenovoLaptop);

console.log(lenovoLaptop.name);
console.log(lenovoLaptop.manufactureDate);

let myLaptop = new Laptop("MacBook Air", 2020);

console.log(myLaptop);

let oldLaptop = new Laptop("Portal R2E CCMC", 1980);
console.log(oldLaptop);


let computer = {};

let myComputer = new Object();


console.log(computer);


// [SECTION] Accessing Object Properties

// Using dot notation
console.log("Result from dot notation: " + myLaptop.name);
console.log("Result from dot notationi: " + cellphone.name);


// Using square braket notation
console.log("Result from dot notation: " + myLaptop["name"]);
console.log("Result from dot notationi: " + cellphone["name"]);
